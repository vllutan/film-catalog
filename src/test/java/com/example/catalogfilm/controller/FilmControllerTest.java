package com.example.catalogfilm.controller;

import com.example.catalogfilm.constants.GenreEnum;
import com.example.catalogfilm.model.Film;
import com.example.catalogfilm.service.FilmService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(FilmController.class)
public class FilmControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FilmService filmService;

    @Test
    public void addFilmTest() throws Exception {
        Film film = new Film();
        film.setTitle("film 1");
        film.setGenre(GenreEnum.HORROR);
        film.setRating(7);

        when(filmService.saveFilm(any())).thenReturn(film);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(film);

        this.mockMvc.perform(post("/film")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .characterEncoding("utf-8"))
                .andExpect(status().isOk());
    }


    @Test
    public void getFilmTest() throws Exception{
        Film film = new Film();
        film.setUuid(UUID.randomUUID());
        film.setTitle("film 1");
        film.setGenre(GenreEnum.HORROR);
        film.setRating(7);

        when(filmService.getFilm(film.getUuid())).thenReturn(film);

        this.mockMvc.perform(get("/film")
                .param("filmUuid", String.valueOf(film.getUuid())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title").value(film.getTitle()))
                .andExpect(jsonPath("$.rating").value(film.getRating()))
                .andExpect(jsonPath("$.genre").value(film.getGenre().toString()));
    }
}
