package com.example.catalogfilm.controller;

import com.example.catalogfilm.constants.GenreEnum;
import com.example.catalogfilm.model.Director;
import com.example.catalogfilm.model.Film;
import com.example.catalogfilm.service.DirectorService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DirectorController.class)
public class DirectorControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    DirectorService directorService;

    @Test
    public void addDirectorTest() throws Exception{
        Film film = new Film();
        film.setTitle("film 1");
        film.setGenre(GenreEnum.HORROR);
        film.setRating(7);
        List<Film> filmList = List.of(film);

        Director director = new Director();
        director.setName("director1");
        director.setAge(22);
        director.setCountry("Russia");
        director.setFilmList(filmList);

        when(directorService.saveDirector(director)).thenReturn(director);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(director);

        this.mockMvc.perform(post("/director")
                .content(json)
                .contentType("application/json")
                .characterEncoding("utf-8"))
                .andExpect(status().isOk());
    }

    @Test
    public void getDirectorTest() throws Exception{
        Film film = new Film();
        film.setTitle("film 1");
        film.setGenre(GenreEnum.HORROR);
        film.setRating(7);
        List<Film> filmList = List.of(film);
        ObjectMapper objectMapper = new ObjectMapper();
        String filmListJson = objectMapper.writeValueAsString(filmList);
        System.out.println(filmListJson);

        Director director = new Director();
        director.setUuid(UUID.randomUUID());
        director.setName("director1");
        director.setAge(22);
        director.setCountry("Russia");
        director.setFilmList(filmList);

        System.out.println(objectMapper.writeValueAsString(director.getFilmList()));
        when(directorService.getDirector(director.getUuid())).thenReturn(director);

        this.mockMvc.perform(get("/director")
                .param("directorUuid", director.getUuid().toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(director.getName()))
                .andExpect(jsonPath("$.age").value(director.getAge()))
                .andExpect(jsonPath("$.country").value(director.getCountry()))
                .andExpect(jsonPath("$.filmList").isNotEmpty());
    }
}
