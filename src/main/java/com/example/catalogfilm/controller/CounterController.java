package com.example.catalogfilm.controller;

import com.example.catalogfilm.service.AsyncService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

@RequiredArgsConstructor
@RestController
public class CounterController {

    private final AsyncService asyncService;

    @GetMapping("/counter")
    public String startThreading() throws InterruptedException, ExecutionException {

        List<CompletableFuture<Integer>> sum = new ArrayList<>();

        for(int i=0; i<3; ++i) {
            sum.add(asyncService.addCounter(i));
        }

        var result = sum.stream().map(elem -> {
            try {
                return elem.get();
            } catch (InterruptedException | ExecutionException e) {
                throw new RuntimeException(e);
            }
        }).max(Integer::compare).get();
        System.out.println("total: " + result);
        System.out.println("what returned:\n1 - " + sum.get(0).get() +
                ", 2 - " + sum.get(1).get() + ", 3 - " + sum.get(2).get());

        return "ok";
    }
}
