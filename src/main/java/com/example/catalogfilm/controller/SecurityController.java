package com.example.catalogfilm.controller;

import com.example.catalogfilm.service.GreetingService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@RequiredArgsConstructor
@Controller
@RequestMapping("/secure")
public class SecurityController {

    private final GreetingService greetingService;

    @GetMapping("/public")
    public String publicAccess(){
        return "publicTemplate";
    }

    @GetMapping("/user")
    public String userAccess(Model model, Principal principal){
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        model.addAttribute("authenticatedUsername", userDetails.getUsername());
        model.addAttribute("greeting", greetingService.getGreeting(principal.getName()));
        return "userTemplate";
    }

    @GetMapping("/admin")
    public String adminAccess(){
        return "adminTemplate";
    }

}
