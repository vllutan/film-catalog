package com.example.catalogfilm.service;

public interface CounterService {
    void increment();

    Integer getValue();
}
