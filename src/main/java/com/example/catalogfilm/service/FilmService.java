package com.example.catalogfilm.service;

import java.util.UUID;
import com.example.catalogfilm.model.Film;

public interface FilmService {

    Film getFilm(UUID filmUuid);
    Film saveFilm(Film film);
}
