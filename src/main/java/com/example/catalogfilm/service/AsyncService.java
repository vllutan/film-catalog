package com.example.catalogfilm.service;

import java.util.concurrent.CompletableFuture;

public interface AsyncService {

    CompletableFuture<Integer> addCounter(int num);
}
