package com.example.catalogfilm.service;

public interface GreetingService {
    String getGreeting(String username);
    String defaultGreeting(String username, Exception e);
}
