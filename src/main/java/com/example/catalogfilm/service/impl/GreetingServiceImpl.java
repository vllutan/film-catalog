package com.example.catalogfilm.service.impl;

import com.example.catalogfilm.service.GreetingService;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class GreetingServiceImpl implements GreetingService {

    @CircuitBreaker(name = "CircuitBreakerService", fallbackMethod = "defaultGreeting")
    @Override
    public String getGreeting(String username) {
        return new RestTemplate()
                .getForObject("http://localhost:8888/greeting/{username}",
                        String.class, username);
    }

    @Override
    public String defaultGreeting(String username, Exception e) {
        return "Welcome.";
    }

}
