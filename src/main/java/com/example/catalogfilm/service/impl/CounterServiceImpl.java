package com.example.catalogfilm.service.impl;

import com.example.catalogfilm.service.CounterService;
import org.springframework.stereotype.Service;

@Service
public class CounterServiceImpl implements CounterService {

    private static Integer counter = 0;

    @Override
    public void increment() {
        ++counter;
    }

    @Override
    public Integer getValue() {
        return counter;
    }
}
