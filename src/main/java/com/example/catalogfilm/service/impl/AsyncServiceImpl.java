package com.example.catalogfilm.service.impl;

import com.example.catalogfilm.service.AsyncService;
import com.example.catalogfilm.service.CounterService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
@RequiredArgsConstructor
public class AsyncServiceImpl implements AsyncService {

    private final CounterService counterService;

    @Async
    @Override
    public CompletableFuture<Integer> addCounter(int num) {
        for (int i = 0; i < 1000000; ++i) {
            synchronized (CounterService.class) {
                counterService.increment();
                if (i % 200000 == 0) {
                    System.out.println("Thread " + num + ", iteration " + i +
                            ", counter value  " + counterService.getValue());
                }
            }
        }

        return CompletableFuture.completedFuture(counterService.getValue());
    }
}
