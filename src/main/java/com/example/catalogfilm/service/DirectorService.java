package com.example.catalogfilm.service;

import java.util.UUID;
import com.example.catalogfilm.model.Director;

public interface DirectorService {

    Director getDirector(UUID directorUuid);

    Director saveDirector(Director director);

}
